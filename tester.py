# -*- coding: utf-8 -*- #
#!/usr/bin/python

import urllib2
import os
import socket

url_base = "www.receita.fazenda.gov.br/PessoaJuridica/CNPJ/cnpjreva/cnpjreva_solicitacao2.asp";
proxy_file = os.getcwd() + "/proxies.txt"

def getRequests(proxy):
    req = urllib2.Request(url_base)
    req.set_proxy(proxy, "http")
    try:
        r = urllib2.urlopen(req, timeout=4)
        return r.getcode()
    except urllib2.HTTPError, e:
        return e.code
        pass
    except urllib2.URLError, e:
        return e.reason
        pass
    except socket.timeout, e:
        return e.message
        pass
    except Exception, e:
        print e.message

def getProxies():
    return open(proxy_file).read()

def main():
    proxies = getProxies().split('\n')
    erros = ["timed out", "400"]
    for proxy in proxies:
        status = str(getRequests(proxy))
        if status in erros:
            print proxy + " - " + status
        else:
            print "Tester has been successfullgit " + status

main()
